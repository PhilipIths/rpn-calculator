//
//  ViewController.m
//  RPN-Calculator
//
//  Created by Stjernström on 2015-03-26.
//  Copyright (c) 2015 Stjernström. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (nonatomic) NSString *currentNumber;
@property (nonatomic) NSMutableArray *list;
@property (weak, nonatomic) IBOutlet UIButton *pointButton;

@end

@implementation ViewController

// Numpad
- (IBAction)button0:(id)sender {
    [self enterNumber:@"0"];
}
- (IBAction)button1:(id)sender {
    [self enterNumber:@"1"];
}
- (IBAction)button2:(id)sender {
    [self enterNumber:@"2"];
}
- (IBAction)button3:(id)sender {
    [self enterNumber:@"3"];
}
- (IBAction)button4:(id)sender {
    [self enterNumber:@"4"];
}
- (IBAction)button5:(id)sender {
    [self enterNumber:@"5"];
}
- (IBAction)button6:(id)sender {
    [self enterNumber:@"6"];
}
- (IBAction)button7:(id)sender {
    [self enterNumber:@"7"];
}
- (IBAction)button8:(id)sender {
    [self enterNumber:@"8"];
}
- (IBAction)button9:(id)sender {
    [self enterNumber:@"9"];
}
- (IBAction)buttonPoint:(id)sender {
    BOOL hasPoint = NO;
    
    for (int i = 0; i < [self.currentNumber length]; i++) {
        if ([[self.currentNumber substringWithRange:NSMakeRange(i, 1)] isEqual:@"."]) {
            hasPoint = YES;
            break;
        }
    }
    
    if (!hasPoint) {
        [self enterNumber:@"."];
    }
}

// Enter number
- (IBAction)buttonEnter:(id)sender {
    // Checks and removes unused point in string
    if ([[self.currentNumber substringWithRange:NSMakeRange([self.currentNumber length] - 1, 1)] isEqual:@"."]) {
        self.currentNumber = [self.currentNumber stringByReplacingOccurrencesOfString:@"." withString:@""];
        self.list[self.list.count - 1] = self.currentNumber;
    }
    self.currentNumber = @"0";
    [self.list addObject:self.currentNumber];
    [self.table reloadData];
}

// Clear table
- (IBAction)buttonClear:(id)sender {
    while (self.list.count > 1) {
        [self.list removeObjectAtIndex:1];
    }
    self.currentNumber = @"0";
    self.list[0] = self.currentNumber;
    [self.table reloadData];
    NSLog(@"Table cleared");
}

// Calculation-functions
- (IBAction)buttonAdd:(id)sender {
    if (self.list.count > 1) {
        [self runCalculation:@"+"];
        [self.table reloadData];
    }
}
- (IBAction)buttonSubtract:(id)sender {
    if (self.list.count > 1) {
        [self runCalculation:@"-"];
        [self.table reloadData];
    }
}
- (IBAction)buttonMultiply:(id)sender {
    if (self.list.count > 1) {
        [self runCalculation:@"*"];
        [self.table reloadData];
    }
}
- (IBAction)buttonDivide:(id)sender {
    if (self.list.count > 1) {
        [self runCalculation:@"/"];
        [self.table reloadData];
    }
}

// Calculation-functions - All
- (IBAction)buttonAddAll:(id)sender {
    if (self.list.count > 1) {
        while (self.list.count > 1) {
            [self runCalculation:@"+"];
        }
        [self.table reloadData];
    }
}
- (IBAction)buttonSubtractAll:(id)sender {
    if (self.list.count > 1) {
        while (self.list.count > 1) {
            [self runCalculation:@"-"];
        }
        [self.table reloadData];
    }
}
- (IBAction)buttonMultiplyAll:(id)sender {
    if (self.list.count > 1) {
        while (self.list.count > 1) {
            [self runCalculation:@"*"];
        }
        [self.table reloadData];
    }
}
- (IBAction)buttonDivideAll:(id)sender {
    if (self.list.count > 1) {
        while (self.list.count > 1) {
            [self runCalculation:@"/"];
        }
        [self.table reloadData];
    }
}

// Extra Functions
- (IBAction)buttonPlusMinusSwitch:(id)sender {
    double switchNum = [self.currentNumber doubleValue];
    switchNum *= -1;
    self.currentNumber = [NSString stringWithFormat:@"%g", switchNum];
    self.list[self.list.count - 1] = self.currentNumber;
    [self.table reloadData];
}

- (IBAction)buttonPercentage:(id)sender {
    if (self.list.count > 1) {
        [self runCalculation:@"%"];
        [self.table reloadData];
    }
}

// Methods
- (void)enterNumber:(NSString *)number {
    
    if ([self.currentNumber isEqual:@"0"] && ![number isEqual:@"."]) {
        self.currentNumber = [NSString stringWithFormat:@"%@", number];
    }
    
    else {
        self.currentNumber = [NSString stringWithFormat:@"%@%@", self.currentNumber, number];
    }
    
    NSLog(@"%@", self.currentNumber);
    
    self.list[self.list.count - 1] = self.currentNumber;
    [self.table reloadData];
}

- (void)runCalculation:(NSString*)function {
    // Applies last two entries in list to respective double values
    double initValue = [self.list[self.list.count - 2] doubleValue];
    double numToAlter = [self.list[self.list.count - 1] doubleValue];
    double result;
    
    // Get character from argument to determine if-path
    if ([function isEqual:@"+"]) {
        result = initValue + numToAlter;
    }
    else if ([function isEqual:@"-"]) {
        result = initValue - numToAlter;
    }
    else if ([function isEqual:@"*"]) {
        result = initValue * numToAlter;
    }
    else if ([function isEqual:@"/"]) {
        result = initValue / numToAlter;
    }
    else if ([function isEqual:@"%"]) {
        double temp = 0;
        result = 0;
        
        if (initValue > numToAlter) {
            
            while (initValue > temp) {
                temp += numToAlter;
                
                if (temp > initValue) {
                    temp -= numToAlter;
                    NSLog(@"temp: %g", temp);
                    double temp2 = initValue - temp;
                    NSLog(@"temp2: %g", temp);
                    result += (temp2 / numToAlter) * 100;
                    break;
                }
                result += 100;
                NSLog(@"Adding temp: %g", temp);
            }
        }
        
        else {
            result = (initValue / numToAlter) * 100;
            NSLog(@"Adding temp: %g", (initValue / numToAlter));
        }
    }
    
    // Clears the last entry and replaces the new last entry with result 
    [self.list removeObjectAtIndex:self.list.count - 1];
    self.currentNumber = [NSString stringWithFormat:@"%g", result];
    self.list[self.list.count - 1] = self.currentNumber;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return self.list.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.list[indexPath.row];
    
    return cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.list = [[NSMutableArray alloc] init];
    self.currentNumber = @"0";
    [self.list addObject:self.currentNumber];
    self.table.delegate = self;
    self.table.dataSource = self;
    [self.table reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
